import React, { Component } from 'react';
import {Card, CardImg, CardImgOverlay, CardText, CardBody, CardTitle} from 'reactstrap';
import Menu from './MenuComponent';

class DishDetail extends Component {

  constructor(props){
    super(props);
    this.state = {
dishdetails:null
    }
  }

  render() {
    const { dishdetails } = this.props;

    if (dishdetails != null){

      return(
        <div className = "row">
          <Card>
            <CardImg width="100%" src={dishdetails.image} alt={dishdetails.name} />
            <CardBody>
              <CardTitle> {dishdetails.name} </CardTitle>
              <CardText> {dishdetails.description}</CardText>
            </CardBody>
          </Card>

        </div>
      );
    }

    else {
      return(
        <div> </div>
      );

    }
  }
}
export default DishDetail;
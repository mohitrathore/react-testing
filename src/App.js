import React, { Component } from 'react';
import logo from './logo.svg';
import {Navbar, NavbarBrand, Media} from 'reactstrap';
import Menu from './components/MenuComponent';
import DishDetail from './components/DishdetailComponent';

import './App.css';
import { DISHES } from './shared/dishes';


class App extends Component {
  constructor(props){

    super(props);
    this.state={
      dishes:DISHES
    }
  }
  render(){
  return (
    <div>
    <Navbar dark color='secondary'>  
   <div className="container">
   <NavbarBrand href="/"> <Media object src="assets/images/IMG_20191119_194053.jpg" width="15%"/> Hello this is my first react code </NavbarBrand>

   </div>

    </Navbar>
    <Menu dishes={this.state.dishes} />
    <DishDetail />
    </div>
  );
}
}
export default App;
